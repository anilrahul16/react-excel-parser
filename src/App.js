import './App.scss';
import React from 'react';
import FileUploader from './components/FileUploader';
import ExportReport from './components/ExportReport';
import { AgGridReact } from 'ag-grid-react';
import Select from 'react-select';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.setViewData = this.setViewData.bind(this);

    this.state = {
      selectOptions: [],
      id: '',
      name: '',
      parsedData: [],
      columnDefs: [],
      defaultColDef: {
        width: 250,
        editable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
      },
      rowData: null,
    }

  }

  clearView = () => {

    this.setState({
      selectOptions: [],
      rowData: null,
      parsedData: [],
      columnDefs: [],
      id: '',
      name: ''
    });
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  getColumnDefs(data) {
    let columDefs = [];
    if (data && data.length > 0) {
      let keys = Object.keys(data[0]);
      for (let columnDef of keys) {
        columDefs.push({ 'headerName': columnDef, 'field': columnDef });
      }
    }


    return columDefs;
  }

  setViewData(val) {
    if (!Array.isArray(val)) {
      let options = Object.keys(val);
      let select = [];
      for (let option of options) {
        select.push({ 'value': option, 'label': option });
      }
      this.setState({
        selectOptions: select,
        parsedData: Object.assign({}, val)
      });
      this.handleGrid({ 'value': options[0], 'label': options[0] })
    } else {
      this.clearView();
    }
  }

  handleChange = (e) => {
    this.handleGrid(e);
  }

  handleGrid = (select) => {
    this.setState({ id: select.value, name: select.label });
    let data = this.state.parsedData ? this.state.parsedData[select.label] : [];
    let colDefs = data ? this.getColumnDefs(data) : [];
    this.setState({ columnDefs: colDefs, rowData: data });
  }


  render() {

    return (
      <div className="App">
        <FileUploader onParseData={this.setViewData} />
        <div className="dropDownSelect">
          <Select options={this.state.selectOptions} onChange={this.handleChange.bind(this)} />
          <p>You have selected <strong>{this.state.name}</strong> whose id is <strong>{this.state.id}</strong></p>
        </div>
        { this.state.rowData && <div>
          <ExportReport reportData={this.state.parsedData} />
          <div style={{ height: '600px', boxSizing: 'border-box', clear: 'both' }}>
            <div
              style={{
                height: '100%',
                width: '100%',
              }}
              className="ag-theme-alpine"
            >
              <AgGridReact
                columnDefs={this.state.columnDefs}
                defaultColDef={this.state.defaultColDef}
                rowData={this.state.rowData}
                onGridReady={this.onGridReady}
              />
            </div>
          </div>
        </div>
        }
      </div>
    );
  }
};

export default App;
