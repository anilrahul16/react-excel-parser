import { Component } from "react";
import xlsxParser from 'xlsx-parse-json';
import Alert from 'react-bootstrap/Alert';
import { Button } from 'react-bootstrap';

class FileUploader extends Component {

  constructor(props) {
    super(props);
    this.state = {
      inputKey: ''
    }
  }

  // On file select (from the pop up) 
  onFileChange = event => {

    // Update the state 
    let fileObj = event.target.files[0];

    xlsxParser.onFileSelection(fileObj).then(data => {
      this.props.onParseData(data);
    });

  };

  resetsFileInput = () => {

    this.setState({
      inputKey: Date.now()
    });

    this.props.onParseData([]);
  }


  render() {

    return (
      <div>
        <Alert variant="success">
          <Alert.Heading>File Upload using React</Alert.Heading>
          <p>
            Please upload Excel file, once the excel is uploaded the data is automatically parsed and
            displayed below in the grid. Feel free to play with the grid and you have the flexibility to manipulate the data.
          </p>
          <hr />
          <p className="mb-0">
            Also, Clear button is provided to clear and start a fresh upload of Excel.
          </p>
        </Alert>
        <div>
          <input type="file" onChange={this.onFileChange} key={this.state.inputKey} />
          <Button variant="warning" onClick={this.resetsFileInput}>Clear</Button>
        </div>
        <br />
      </div>
    );
  }
}


export default FileUploader;