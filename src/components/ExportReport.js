import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import pptxgen from "pptxgenjs";


class ExportReport extends Component {

    constructor(props) {
        super(props);
    }

    exportToPPT = () => {
        let pptx = new pptxgen();

        let slide = pptx.addSlide();
        this.createComputeReport(pptx, slide);

        pptx.writeFile("EC2_Report.pptx");
    }

    createSlideText = (pptx, slide) => {

        let textboxText = 'Compute';
        let textboxOpts = {
            x: 0.2,
            y: 0.3,
            color: '338EFF',
            align: pptx.AlignH.left
        };
        slide.addText(textboxText, textboxOpts);
    }

    createComputeReport = (pptx, slide) => {
        this.createSlideText(pptx, slide);
        this.createSummaryTable(slide);
        this.loadBalancerTable(slide);
        this.networkInterfacesTable(slide);
        this.ec2VolumesTable(slide);
        this.orphanedVolumesTable(slide);
        this.ec2InstanceTable(slide);
        this.createInstanceReport(pptx, slide, 'Instance Type', {
            x: 3.65,
            y: 2.95,
            w: '50%',
            chartColors	: ['4d79ff']
        });
    }

    createSummaryTable = (slide) => {
        let rows = [];
        rows.push([
            { text: 'Summary', options: { bold: true } },
            { text: 'Count', options: { bold: true } }
        ]);
        rows.push(['Active Regions', this.totalCountsMap('EC2 Instances', 'Region', false)]);
        rows.push(['Availability Zones', this.totalCountsMap('EC2 Instances', 'Availability Zone', false)]);
        rows.push(['EC2 Instance ID', this.totalCountsMap('EC2 Instances', 'EC2 Instance ID', false)]);
        rows.push(['EC2 Volumes (EBS)', this.totalCountsMap('EC2 Volumes', 'Volume ID', false)]);
        rows.push(['EC2 Orphaned Volumes (EBS)', this.totalCountsMap('Orphaned Volumes', 'VolumeId', false)]);
        rows.push(['EC2 Network Interfaces', this.totalCountsMap('EC2 Network interfaces', 'EC2 Instance ID', false)]);
        rows.push(['EC2 Load Balancers', this.totalCountsMap('EC2 Load Balancers', 'Load Balancer Name', false)]);
        slide.addTable(rows, { x: 0.5, y: 0.5, w: 3.0, h: 2, colW: [2.3, 0.5], color: "363636", border: { pt: '1', color: 'f1f1f1' } });
    }

    loadBalancerTable = (slide) => {
        let rows = [];
        let loadBalancers = this.getCountsMap('EC2 Load Balancers', 'Load Balancer Type');
        rows.push([
            { text: 'Load Balancer', options: { bold: true } },
            { text: 'Count', options: { bold: true } }
        ]);
        rows.push(['Network Load Balancers', loadBalancers.get('network') ? loadBalancers.get('network') : 0]);
        rows.push(['Application Load Balancers', loadBalancers.get('application') ? loadBalancers.get('application') : 0]);
        rows.push(['Classic Load Balancers', loadBalancers.get('classic') ? loadBalancers.get('classic') : 0]);
        slide.addTable(rows, { x: 3.65, y: 0.5, w: 2.5, h: 1, colW: [2.0, 0.5], color: "363636", border: { pt: '1', color: 'f1f1f1' } });
    }

    networkInterfacesTable = (slide) => {
        let rows = [];
        rows.push([
            { text: 'Network Interface', options: { bold: true } },
            { text: 'Count', options: { bold: true } }
        ]);
        rows.push(['Subnets', this.getCountsMap('EC2 Network interfaces', 'Subnet ID').size]);
        rows.push(['VPCs', this.getCountsMap('EC2 Network interfaces', 'VPC ID').size]);
        slide.addTable(rows, { x: 3.65, y: 1.8, w: 3.0, h: 1, colW: [2.0, 0.5], color: "363636", border: { pt: '1', color: 'f1f1f1' } });
    }


    ec2VolumesTable = (slide) => {
        let rows = [];
        let ec2Volumes = this.getCountsMap('EC2 Volumes', 'State');
        let ec2VolumesBoot = this.getCountsMap('EC2 Volumes', 'IsBoot');
        let ec2VolumesEncypted = this.getCountsMap('EC2 Volumes', 'Encrypted');

        rows.push([
            { text: 'EC2 Volumes (EBS)', options: { bold: true } },
            { text: 'Values', options: { bold: true } }
        ]);
        rows.push(['Volumes attached to Stopped EC2', ec2Volumes.get('stopped') ? ec2Volumes.get('stopped') : 0]);
        rows.push(['Boot Volumes', ec2VolumesBoot.get('True') ? ec2VolumesBoot.get('True') : 0]);
        rows.push(['Total Volumes Capacity (GB)', this.getTotalCapacity('EC2 Volumes', 'Capacity (MB)')]);
        rows.push(['Ratio of Encrypted Volumes', (ec2VolumesEncypted.get('True') ? ec2VolumesEncypted.get('True') : 0) + ':' + (ec2VolumesEncypted.get('False') ? ec2VolumesEncypted.get('False') : 0)]);
        slide.addTable(rows, { x: 6.5, y: 0.5, w: 3.0, h: 1, colW: [2.4, 0.6], color: "363636", border: { pt: '1', color: 'f1f1f1' } });
    }

    orphanedVolumesTable = (slide) => {
        let rows = [];
        rows.push([
            { text: 'Orphaned Volumes (EBS)', options: { bold: true } },
            { text: 'Values', options: { bold: true } }
        ]);
        rows.push(['Volumes Count', this.getCountsMap('Orphaned Volumes', 'VolumeId').size]);
        rows.push(['Total Volumes Capacity (GB)', this.getTotalCapacity('Orphaned Volumes', 'Capacity (MB)')]);
        slide.addTable(rows, { x: 6.5, y: 2.0, w: 3.0, h: 1, colW: [2.4, 0.6], color: "363636", border: { pt: '1', color: 'f1f1f1' } });
    }


    ec2InstanceTable = (slide) => {
        let rows = [];
        rows.push([
            { text: 'EC2 Instances', options: { bold: true } },
            { text: 'Value', options: { bold: true } }
        ]);

        let stateCounts = this.getCountsMap('EC2 Instances', 'State');
        rows.push(['Total Running', stateCounts.get('running') ? stateCounts.get('running') : 0]);
        rows.push(['Total Stopped', stateCounts.get('stopped') ? stateCounts.get('stopped') : 0]);

        let instancesMap = this.getCountsMap('EC2 Instances', 'Instance Type');
        rows.push(['Unique Instances', this.getUniqueInstanceCount(instancesMap, 1)]);
        rows.push(['Instance T-Shirt Sizes', instancesMap.size]);
        rows.push(['Total Cores', this.totalCountsMap('EC2 Instances', 'CPU Cores', true)]);
        rows.push(['Total Memory (GB)', this.getTotalCapacity('EC2 Instances', 'Memory (MB)')]);
        let totalDiskUsedCapacity = this.getTotalCapacity('EC2 Instances', 'Disk Used Capacity (MB)');
        rows.push(['Total Disk Used Capacity (GB)', totalDiskUsedCapacity]);
        rows.push(['Total Free Capacity (GB)', (this.getTotalCapacity('EC2 Volumes', 'Capacity (MB)') - totalDiskUsedCapacity).toFixed(2)]);
        slide.addTable(rows, { x: 0.5, y: 3.0, w: 3.0, h: 1, colW: [2.2, 0.6], color: "363636", border: { pt: '1', color: 'f1f1f1' } });
    }

    getUniqueInstanceCount = (uniqueInstancesMap, searchValue) => {
        let uniqueInstanceCount = [];
        for (let [key, value] of uniqueInstancesMap.entries()) {
            if (value === searchValue) {
                uniqueInstanceCount.push(key);
            }
        }
        return uniqueInstanceCount.length;
    }

    createInstanceReport = (pptx, slide, typeKey, slideOptions) => {
        let typesMap = this.getCountsMap('EC2 Instances', typeKey);
        this.createBarChart(pptx, slide, typesMap, slideOptions);
    }

    totalCountsMap = (instanceKey, typeKey, isNumber) => {
        let totalCount = 0;
        if (!isNumber) {
            let countsMap = this.getCountsMap(instanceKey, typeKey);
            for (let value of countsMap.values()) {
                totalCount += value;
            }
        } else {
            let reportData = this.props.reportData[instanceKey];
            for (let instance of reportData) {
                totalCount += parseFloat(instance[typeKey]);
            }
        }
        return totalCount;
    }

    getBooleanCounts = (instanceKey, typeKey, value) => {
        let countsMap = this.getCountsMap(instanceKey, typeKey);
        return countsMap.get();
    }

    getTotalCapacity = (instanceKey, capacityField) => {
        let totalCapacity = 0;
        let reportData = this.props.reportData[instanceKey];
        for (let instance of reportData) {
            totalCapacity += parseFloat(instance[capacityField]);
        }
        totalCapacity = (totalCapacity / 1024).toFixed(2);
        return totalCapacity;
    }


    getCountsMap = (instanceKey, typeKey) => {
        let typesMap = new Map();
        let reportData = this.props.reportData[instanceKey];
        for (let instance of reportData) {

            let key = instance[typeKey];

            if (key) {
                if (typesMap.has(key)) {
                    let count = typesMap.get(key);
                    count = count + 1;
                    typesMap.set(key, count);
                } else {
                    let instanceCount = 1;
                    typesMap.set(key, instanceCount);
                }
            }
        }

        return typesMap;

    }

    createChartEntries = (typesMap) => {

        let chart = {
            labels: [],
            values: []
        }


        for (let entry of typesMap.entries()) {
            chart.labels.push(entry[0]);
            chart.values.push(entry[1]);
        }

        return [
            {
                name: this.props.reportKey,
                labels: chart.labels,
                values: chart.values
            }
        ];

    }

    createBarChart = (pptx, slide, typesMap, slideOptions) => {
        slide.addChart(pptx.charts.BAR, this.createChartEntries(typesMap), slideOptions);
    }

    createPieChart = (pptx, slide, typesMap, slideOptions) => {
        slide.addChart(pptx.charts.PIE, this.createChartEntries(typesMap), slideOptions);
    }

    render() {
        return (
            <div className="export">
                <Button variant="success" onClick={this.exportToPPT} >Export</Button>
            </div>
        )
    }
}

export default ExportReport;
